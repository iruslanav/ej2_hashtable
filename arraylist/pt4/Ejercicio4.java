package pt4;
/**
Combina los m�todos generados en las actividades 2 y 3
creando una aplicaci�n que gestione ventas y control de stock en
una misma interfaz. Utiliza para ello las estructuras de datos que
creas conveniente.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import javax.swing.JOptionPane;

public class Ejercicio4 {

	/**
	 * @param args
	 */
	// arraylist con arrays de String
	static ArrayList<String[]> pelis = new ArrayList<>();
	static int caja = 1000;

	public static void main(String[] args) {
		// assignamos un valores a pelis con un array de 3 posiciones
		// (nombre,precio,cantidad)
		pelis.add(new String[] { "el sicario de dios", "60", "10" });
		pelis.add(new String[] { "orgullo y prejuicio", "80", "5" });
		pelis.add(new String[] { "el se�or de los anillos", "100", "20" });
		pelis.add(new String[] { "avengers", "100", "15" });
		pelis.add(new String[] { "tomb raider", "20", "5" });
		pelis.add(new String[] { "harry potter", "30", "15" });
		pelis.add(new String[] { "star wars", "75", "50" });
		pelis.add(new String[] { "ice age", "60", "10" });
		pelis.add(new String[] { "fast and furious", "40", "25" });
		pelis.add(new String[] { "gorrion rojo", "50", "15" });
		// metodo principal que nos muestra la lista de pelis y las opciones
		indice();
	}

	// metodos
	// lista todos los productos, sale siempre al inicio
	private static void indice() {

		// variables
		String opcion = "";
		boolean salir = false;

		// switch que llama a los m�todos seg�n la opci�n que elijas
		while (!salir) {
			// metodo nombrePeliculas para listar las peliculas dentro del JoptioPane, la
			// ventana guarda lo que introduce por teclado
			opcion = JOptionPane.showInputDialog(null,
					"Disponemos de las siguientes peliculas:\n" + listaPeliculas()
							+ "\n1) Compras\n2) Ventas\n3) Actualizar\n4) Eliminar\n5) Salir\n\nCaja: " + caja
							+ " �\nQue quieres hacer:");

			// menu principal
			switch (opcion) {
			// compras
			case "1":
				compras();
				break;
			// ventas
			case "2":
				ventas();
				break;
			case "3":
				actualizar();
				break;
			case "4":
				eliminar();
				break;
			// salir
			case "5":
				salir = true;
				break;
			default:
				JOptionPane.showMessageDialog(null, "Introduce una opci�n v�lida");
				break;
			}

		}

	}

	// devuelve un string con toda la informacio sobre las pelis
	private static String listaPeliculas() {
		// muestras los productos como una lista
		String npi = "";// aqui guardaremos los nombres de los productos.
		// bucle foreach para mostrar nombre y cantidad
		for (String[] strings : pelis) {
			npi += "> " + strings[0] + "\n" + strings[1] + " �/u  " + strings[2] + " unidades\n";
		}
		return npi;
	}

	// a�ade una peli al stock
	private static void compras() {
		//
		String nombre = JOptionPane.showInputDialog(null, "Introduce el nombre de la pelicula");
		String precio = JOptionPane.showInputDialog(null, "Introduce el precio de la pelicula");
		String cantidad = JOptionPane.showInputDialog(null, "Introduce la cantidad");
		// restar a la caja la multiplicacion de precio por cantidad pasados a int
		caja -= (Integer.parseInt(precio) * Integer.parseInt(cantidad));
		// guardar en un array auxiliar los datos para despues guradarlo en el arraylist
		// de pelis
		String[] pelicula = { nombre, precio, cantidad };
		pelis.add(pelicula);
	}

	// eliminas una cantidad de pelis del stock si existe y si hay suficientes
	private static void ventas() {
		String nombre = JOptionPane.showInputDialog(null, "Introduce el nombre de la pel�cula");
		boolean cantidadDisponible = true;
		// comprobar si existe la pelicula
		if (comprobarNombrePeli(nombre)) {
			String cantidad = "0";
			// bucle do while para evitar que vendas mas peliculas del stock disponible
			do {
				if (!cantidadDisponible) {
					JOptionPane.showMessageDialog(null, "NO HAY SUFICIENTE STOCK!!!");
				}
				// ventana donde muestras el metodo que consulta la cantidad que hay de la peli
				// elegida y pide cuantas quieres
				cantidad = JOptionPane.showInputDialog(null,
						"Cuantas quieres (Disponibles " + consultarCantidadPelis(nombre) + ")");
				// opcional poner una consulta de la cantidad disponible

				// devolvera true si hay suficiente cantidad (stock)
				cantidadDisponible = comprobarActualizarCantidad(cantidad, nombre);
			} while (!cantidadDisponible);

		} else {
			JOptionPane.showMessageDialog(null, "Esta pel�cula NO existe.");
		}
	}

	private static void actualizar() {
		// variables
		int i = 0;
		boolean encontado = false;
	
		// nombre, precio y cantidad a�adida por teclado
		String nombre = JOptionPane.showInputDialog("Que pelicula quieres actualizar");
		// consultar datos
		JOptionPane.showMessageDialog(null,
				"La pelicula tiene estos valores\n" + Arrays.toString(consultarDatos(nombre)));
		String precio = JOptionPane.showInputDialog("Que precio quieres");
		String cantidad = JOptionPane
				.showInputDialog("Que cantidad quieres (Disponibles " + consultarCantidadPelis(nombre) + ")");
		// bucle para comprobar si existe el nombre de la peli
		for (String[] p : pelis) {
	
			// comprobar el nombre de la peli existe
			if (p[0].equalsIgnoreCase(nombre)) {
				encontado = true;
				// hacemos un nuevo array al cual le asignaremos al array que coincide con el
				// nombre de la peli y le modificamos solo la cantidad
				String[] az = { nombre, precio, cantidad };
				// actualizamos en el arraylist pas�ndole el array que hemos creado
				// anteriormente y le decimos tambi�n la posici�n de ese arraylist(i)
				pelis.set(i, az);
			}
			// incremetas la posicion del arraylist
			i++;
		}
	
		if (!encontado) {
			JOptionPane.showMessageDialog(null, "NO existe esta pelicula.");
		}
	}

	private static void eliminar() {
		// variables
		int i = 0;
		boolean encontado = false;
	
		// nombre a�adida por teclado
		String nombre = JOptionPane.showInputDialog("Que pelicula quieres eliminar");
		// bucle para comprobar si existe el nombre de la peli
		for (String[] p : pelis) {
	
			// comprobar el nombre de la peli existe
			if (p[0].equalsIgnoreCase(nombre)) {
				encontado = true;
				// eliminar la pelicula del arraylist
				pelis.remove(i);
				break;
			}
			// incremetas la posicion del arraylist
			i++;
		}
	
		if (!encontado) {
			JOptionPane.showMessageDialog(null, "NO existe esta pelicula.");
		}
	}

	// comprueba si existe el nombnre de la peli
	private static boolean comprobarNombrePeli(String nombre) {
		// variable booleana
		boolean existe = false;
		// guardas los elementos del arraylist
		Iterator<String[]> iterador = pelis.iterator();
		// bucle para comprobar si existe el nombre de la peli
		while (iterador.hasNext()) {
			// coge el parametro nombre y lo comparas con cada una de las peliculas
			// guardadas
			if (nombre.equalsIgnoreCase(iterador.next()[0])) {
				existe = true;
			}
		}
		return existe;
	}

	// metodo que permite averiguar la cantidad de pelis que tienes segun el nombre
	// introducido
	private static String consultarCantidadPelis(String nombre) {
		// variable
		String cantidad = "";
		for (String[] p : pelis) {
			if (nombre.equalsIgnoreCase(p[0])) {
				cantidad = p[2];
			}
		}
		return cantidad;
	}

	private static String[] consultarDatos(String nombre) {
		for (String[] p : pelis) {
			if (nombre.equalsIgnoreCase(p[0])) {
				return p;
			}
		}
		return null;
	}

	// metodo que busca el nombre de la peli y te envia un booleano si puede cambiar
	// la cantidad que tienes en stock
	private static boolean comprobarActualizarCantidad(String cantidad, String nombre) {
		// variables
		boolean hayStock = false;
		int n, c, i = 0;

		// cantidad a�adida por teclado pasa de str a int
		c = Integer.parseInt(cantidad);
		// bucle para comprobar si existe el nombre de la peli
		for (String[] p : pelis) {

			// comprobar el nombre de la peli existe
			if (p[0].equalsIgnoreCase(nombre)) {

				// coge el parametro cant (cantidad en la BBDD) y lo pasamos de str a int
				n = Integer.parseInt(p[2]);
				// comprobar que hay suficiente cantidad en stock
				if (n >= c) {
					hayStock = true;
					// calculo de la diferencia entre el stock y lo que vendes (quitar)
					n -= c;
					// hacemos un nuevo array al cual le asignaremos al array que coincide con el
					// nombre de la peli y le modificamos solo la cantidad
					String[] az = p;
					// modificamos la cantidad de que est� en la posici�n dos
					az[2] = String.valueOf(n);
					// actualizamos en el arraylist pas�ndole el array que hemos creado
					// anteriormente y le decimos tambi�n la posici�n de ese arraylist(i)
					pelis.set(i, az);
					// sumar a la caja la cantidad en int y el precio de la pelicula vendidad pasada
					// de str a int
					caja += (c * Integer.parseInt(p[1]));
				}

			}
			// incremetas la posicion del arraylist
			i++;
		}
		return hayStock;
	}

}
