/**
 * 
 */
package pt2;

import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

/**
 * @author irusl
 *
 */
public class pt2 {

	// variable global
	static double precioConIvaTotal = 0;

	// creamos una hastable
	static Hashtable<String, String> lista = new Hashtable<String, String>();

	public static void main(String[] args) {

		// la lista de productos
		lista.put("patatas", "3");
		lista.put("cacao", "0.75");
		lista.put("leche", "1.5");
		lista.put("cafe", "1.25");
		lista.put("detergente", "6.5");
		lista.put("limpia cristales", "0.90");
		lista.put("escoba", "1.15");
		lista.put("friegasuelos", "2.23");

		// metodo para pedir los productos que quiera hasta que se introduzca la palabra
		// FIN
		pedir_productos();

	}

	private static String nombresproductos() {
		// muestras los productos como una lista

		Enumeration<String> nombre_productos = lista.keys();
		String npi = "\n";// aqui guardaremos los nombres de los productos.

		while (nombre_productos.hasMoreElements()) {
			// coge el nombre de cada posicion, lo guarda en el String npi
			npi += nombre_productos.nextElement() + "\n";

		}
		return npi;
	}

	private static void pedir_productos() {

		DecimalFormat decimal = new DecimalFormat("####.##");
		// lo que mostraremos al final
		String carrito = "";
		// lo pide hasta escribir FIN
		String que = "";
		while (!que.equalsIgnoreCase("fin")) {
			// mostramos productos disponibles
			JOptionPane.showMessageDialog(null, "En la tienda disponemos de: " + nombresproductos());
			// pedimos que producto quiere el cliente
			que = JOptionPane.showInputDialog("Indica qué producto deseas (introduce FIN para acabar): ");
			// Pedimos al programa que no discrimine entre mayusculas y minusculas
			que = que.toLowerCase();
			if (!que.equalsIgnoreCase("fin")) {
				// pedimos cuantos productos quiere
				String cuantos = JOptionPane.showInputDialog("Indica que cantidad de productos deseas comprar: ");
				int num = Integer.parseInt(cuantos);
				// metodo de calcular IVA y nos devuelve un string que lo concatenamos
				carrito += calculoIVA(que, num);

			}

		}
		// Mostramos el IVA / bruto / precio + IVA / n articulos / pagado / cambio

		// dinero recibido por el cliente
		String dinero_recibido = JOptionPane.showInputDialog(carrito + "El precio final es "
				+ decimal.format(precioConIvaTotal) + "\nIndica que cantidad de dinero vas a dar: ");
		double dr = Double.parseDouble(dinero_recibido);

		// cantidad a devolver
		double cambio = dr - precioConIvaTotal;
		JOptionPane.showMessageDialog(null, "Cantidad de dinero pagado por el cliente: " + dr
				+ " \nCantidad de dinero a devolver: " + decimal.format(cambio) + " ");

	}
	// metodo de calcular IVA

	private static String calculoIVA(String Q, int n) {

		DecimalFormat decimal = new DecimalFormat("####.##");
		// variables
		double precioIVA = 0, precioSinIVA = 0;
		String cadena = "";

		switch (Q) {
		case "patatas":
		case "cacao":
		case "leche":
		case "cafe":
			// guardamos el precio del producto
			// cogemos el valor lo pasamos a double
			// precio total sin iva multpilicado por n productos
			precioSinIVA += Double.parseDouble(lista.get(Q)) * n;
			// precio total con iva
			precioIVA += precioSinIVA * 1.04;
			// copiar el subtotal de cada producto de la compra para obtener la suma de
			// precios de todos productos con iva
			precioConIvaTotal += precioIVA;
			cadena = Q + ": IVA= 4%\t Precio sin IVA: " + decimal.format(precioSinIVA) + " \t Precio con IVA: "
					+ decimal.format(precioIVA) + " \t Cantidad: " + n + "\n";
			break;

		case "detergente":
		case "limpia cristales":
		case "friegasuelos":
		case "escoba":
			// guardamos el precio del producto
			// cogemos el valor lo pasamos a double
			// precio total sin iva multpilicado por n productos
			precioSinIVA += Double.parseDouble(lista.get(Q)) * n;
			// precio total con iva
			precioIVA += precioSinIVA * 1.21;
			// copiar el subtotal de cada producto de la compra para obtener la suma de
			// precios de todos productos con iva
			precioConIvaTotal += precioIVA;
			cadena = Q + ": IVA= 21%\t Precio sin IVA: " + decimal.format(precioSinIVA) + " \t Precio con IVA: "
					+ decimal.format(precioIVA) + " \t Cantidad: " + n + "\n";
			break;

		default:
			JOptionPane.showMessageDialog(null, "El producto no existe.");
			break;
		}
		return cadena;

	}

}
