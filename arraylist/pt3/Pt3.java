/**
 * 
 */
package pt3;

import java.util.Enumeration;
import java.util.Hashtable;
import javax.swing.JOptionPane;

/**
 * @author Programacion
 *
 */
public class Pt3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//variables 
		String op = "";
		boolean salir = false;
		
		//hashtable con elementos
		Hashtable<String, String> TablaProductos = new Hashtable<String, String>();
		TablaProductos.put("Sekiro", "60");
		TablaProductos.put("Skyrim", "8");
		TablaProductos.put("The Witcher 3", "20");
		TablaProductos.put("GTA", "30");
		TablaProductos.put("Tomb Raider", "20");
		TablaProductos.put("DOOM", "30");
		TablaProductos.put("Age OF Empires", "15");
		TablaProductos.put("Neverwinter nights", "10");
		TablaProductos.put("Buscaminas", "70");
		TablaProductos.put("Zelda", "40");
		
		//switch que llama a los mtodos segn la opcin que elijas
		while (!salir) {
			op = JOptionPane.showInputDialog(null, "introduce lo que quieres hacer ");
			switch (op.toLowerCase()) {
			case "aadir":
				AadirProducto(TablaProductos);
				break;

			case "consultar":
				consultarProducto(TablaProductos);
				break;

			case "listar":
				listarProducto(TablaProductos);
				break;

			case "borrar":
				borrarProducto(TablaProductos);
				break;

			case "salir":
				salir = true;
				break;
			default:
				JOptionPane.showMessageDialog(null, "introduce una opcin vlida");
				break;
			}

		}
	}
	
	//aade un producto al stock
	private static void AadirProducto(Hashtable<String, String> tablaProductos) {
		// TODO Auto-generated method stub
		String nombre = JOptionPane.showInputDialog(null, "introduce el nombre del producto");
		String precio = JOptionPane.showInputDialog(null, "introduce el precio del producto");
		tablaProductos.put(nombre, precio);
	}
	
	//lista todos los productos
	private static void listarProducto(Hashtable<String, String> tablaProductos) {
		String producto;
		String precio;
		Enumeration<String> enumeration = tablaProductos.keys();
		while (enumeration.hasMoreElements()) {
			producto = enumeration.nextElement();
			precio = tablaProductos.get(producto);
			System.out.println("Producto: " + producto + " Precio: " + precio);

		}

	}
	//consulta un prodcto en especfico
	private static void consultarProducto(Hashtable<String, String> tablaProductos) {
		String nombre = JOptionPane.showInputDialog(null, "introduce el nombre del producto");
		String precio = tablaProductos.get(nombre);
		System.out.println(nombre + " " + tablaProductos.get(nombre));
	}
	//borra un producto
	private static void borrarProducto(Hashtable<String, String> tablaProductos) {
		String nombre = JOptionPane.showInputDialog(null, "introduce el nombre del producto");
		tablaProductos.remove(nombre);
	}


}
